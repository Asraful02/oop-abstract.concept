<html>
<head>
<title>OOP all concept test here</title>
<link rel="stylesheet" type="text/css" a href="style.css">
</head>
<body>
<div class="wrapper">
<?php
abstract class student{
	public $name;
	public $age;
	
	public function details(){
		echo $this->name."is" . $this->age."years old <br>";
		
	}
	
	abstract public function school();
}
	
	class boy extends student{
		
		public function describe(){
			
			return parent::details()."and  i am a high school student.<br>"; 
		}
		
		public function school(){
			
			return "i like a school student.<br>";
		}
	}

$student=new boy();
$student->name="asraful";
$student->age="24";
echo $student->describe();
echo $student->school();
?>
</div>
</body>
</html>